# 风险IP收集列表

#### 介绍
日常扫描我的端口，服务器，路由器，用各种方式手段攻击我的IP地址的收集。大家可以直接放在防火墙里屏蔽它们。

#### 软件架构
只提供一个txt文件，文件中每一行就是一个IP地址。


#### 安装教程

1.  下载txt文件
2.  复制到防火墙屏蔽列表中
3.  保存，生效

#### 使用说明

1.  可以添加到系统防火墙中
2.  也可以添加到硬件防火墙的屏蔽列表中

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
